# Bomberman ORM

<div align="center">
<img width="300" height="400" src="orm.png">
</div>

## Description du projet

Bibliothèque réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Web avancé" durant l'année 2019-2020. \
Le projet ORM (Object-Relational Mapping) a pour but de simplifier l’accès à une base de données SQLite en proposant une approche "objets" plutôt que d’accéder directement à des données relationnelles via des requêtes SQL depuis du code Java.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Benoît DA MOTA : benoit.da-mota@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Web avancé" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 09/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- SQLite ;
- DAO ;
- CRUD ;
- Réflection ;
- Généricité ;
- JDBC.

## Objectif

Le projet ORM (Object-Relational Mapping) a pour but de simplifier l’accès à une base de données SQLite en proposant des "objets" plutôt que d’accéder directement à des données relationnelles depuis du code Java.\
Pour le moment ce projet ne fait que générer automatiquement des tables à partir des définitions des classes. En aucun cas elle peut faire l'opération inverse, à savoir lire
une base de données et lier automatiquement les tables aux classes qui y sont définies.\
A chaque changement de définition d'une classe, il faut recréer la BD afin de ne pas perturber le programme.\
Ce projet s'inspire fortement du framework Hibernate avec les mêmes concepts clés :

*  Utilisation des annotations pour lier dynamiquement un objet à une table d'une base de données.\
En effet, pour identifier les tables et les colonnes d'un objet, des annotations sont utilisées et exploitées par la suite dans le moteur de réflection.\
Ce mécanisme est le fondement même du programme permettant d'analyser les métadonnées d'une classe au runtime (attributs, méthodes, etc.).\
Pour plus amples informations sur ce sujet voir le site de Laurent Guérin sur les concepts avancés Java : [Réfléction](http://www.laurent-guerin.fr/javalangage)

*  Mise en place d'un patron de conception "DAO" (Data Access Object) pour accéder aux opérations d'interrogations classiques d'une table d'une base de données associé à n'importe quel objet.\
4 opérations sont implémentées par défaut : CRUD (create, read, update, delete) :
   - C = Insérer un objet ;
   - R = Récupérer un objet ;
   - U = Mise à jour d'un objet ;
   - D = Suppression d'un objet.
L'utilisation de la générécité permet d'éviter le "cast" et assurer la cohérence des types des objets grâce aux types paramétrables.\
Pour plus amples informations sur ce sujet voir le site de Laurent Guérin sur les concepts avancés Java : [Généricité](http://www.laurent-guerin.fr/javalangage)

<br/> 
   
*  Interrogation du serveur de base de données SQLite avec JDBC (Java Database Connectivity).\
Cette interface permet d'interroger n'importe quelle base de données fournisant un pilote d'accès avec des requêtes natives SQL.\
Pour plus amples informations sur ce sujet voir le site officiel de SQLite : [JDBC SQLite](https://www.sqlitetutorial.net/sqlite-java/)

## Comment ça marche ?

### Ajout référence projet

Pour ajouter la référence de ce projet dans Maven, il faut ajouter les lignes suivantes dans le pom.xml :
```xml
<dependency>
    <groupId>universite.angers.master.info.orm</groupId>
    <artifactId>project-orm</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <scope>compile</scope>
</dependency>
```

### Utilisation

#### Mapper une classe vers une table

Pour mapper un objet Java vers une table d'une BD relationnelle, il faut annoter "<span>@</span>Table" au-dessus de la classe en question
```java
@Table
public class MyClass {
    ...
}
```

Par défaut : 

- Le nom de la table est le nom de la classe. Dans notre exemple c'est "MyClass" qui sera utilisé pour nommé la table. Toutefois nous pouvons changer ce nom en précisant dans le paramétre
de l'annotation "name=my_name", comme ceci :
```java
@Table(name="MyClassChangedName")
public class MyClass {
    ...
}
```

- La classe peut être instanciée. En effet nous pouvons préciser à l'ORM que la classe n'est pas instanciable et qu'elle a pour rôle de factoriser des attributs pour d'autres sous-classes qui hérite la classe.\
Nous pouvons la comparer à l'annotation "MappedSuperclass" d'Hibernate.\
Cela évite de créer les tables "non instanciables" dans la BD.
```java
//La table ne sera pas crée dans la BD. Elle aura juste pour rôle de factoriser des attributs pour d'autres sous-classes.
@Table(isInstanciable=false)
public class MyClass {
    ...
}
```

- La classe ne gére pas automatiquement les clés primaires par auto-incrément. Néanmoins nous pouvons modifier ce comportement en précisant que la classe doit gérer automatiquement l'auto-incrément.\
En effet toute table d'une BD doit fournir une clé primaire obligatoirement.\
Par ailleurs si la classe doit gérer l'auto-incrément, alors on doit lui fournir un attribut de type "integer" qui gardera en mémoire cette valeur auto-incrémentée par la BD.\
L'auto-incrément est réservé exclusivement au nombre. Pas d'autres types (pour le moment).
```java
//La table gére les clés primaires par auto-incrément
@Table(isAutoIncrement=true)
public class MyClass {
    @PrimaryKey //La clé primaire de type int
    private int id;
}
```

#### Mapper un attribut vers une colonne

Pour mapper un attribut Java vers une colonne d'une BD relationnelle, il faut annoter : 
- "<span>@</span>Column" pour les colonnes de types primitives (int, double, etc.).\
Exception : certains objets sont considérés comme des attributs primitives comme String et Date :
```java
@Table
public class Person {
    @Column
    private String nickName;
    
    @Column
    private Date birth;
    
    @Column
    private int age;
    
    ...
}
```

- "<span>@</span>PrimaryKey" pour les colonnes qui vont jouer le rôle de clé primaire :
```java
@Table
public class Person {
    @PrimaryKey
    private String firstName;
    
    @PrimaryKey
    private String lastName;
    
    ...
}
```
Dans cette exemple nous avons deux clés primaires : le nom et le prénom de la personne.

- "<span>@</span>ForeignKey" pour les colonnes qui vont jouer le rôle de clé étrangère. Elle sont réservées pour les objets, tableaux et collections d'objets qui sont complexes.\
En effet chaque objet doit avoir sa propre table annotée "@Table". C'est donc bien une clé étrangère.
```java
@Table
public class Person {
    @ForeignKey
    protected List<Person> goodFriends;
    
    @ForeignKey
    protected Person[] badFriends;
    
    @ForeignKey
    protected Person bestFriend;
    
    ...
}
```

### Enregistrer les tables pour la définition de la base de données

Lorsque tous les objets sont annotés, il faut ensuite les enregistrer dans une "FactoryORM". Un exemple pour créer une base de données unique avec SQLite :
```java
    // On crée la session pour se connecter au serveur de SQLite avec les paramétres de connexion
    SessionSQLite session = new SessionSQLite(CREATE_BD, NAME_BD, realPathBD, PROVIDER_BD, HOST_BD, PORT_BD);
    
    // Contruction de la base de données à partir de la session et des définitions des classes
    FactorySQLite.getInstance()         //Instance unique = singleton
        .addSession(session)            //Ajout de la session
        .addClassObject(Account.class)  //Classe annotée de @Table et des attributs par @Column
        .addClassObject(Game.class)     //Classe annotée de @Table et des attributs par @Column
        .addClassObject(User.class)     //Classe annotée de @Table et des attributs par @Column
        .build();                       //Instanciation "unique" de la base de données. A savoir SQLite
        
    // Ouverture de la base de données afin de l'exploiter
    FactorySQLite.getInstance().getDataBase().open();
    
    // Penser à intialiser la base de données (création des tables, etc.)
    // En effet la "FactoryORM" ne peut pas créer les tables tant que la BD est fermée
    FactorySQLite.getInstance().getDataBase().initialize();
}
```