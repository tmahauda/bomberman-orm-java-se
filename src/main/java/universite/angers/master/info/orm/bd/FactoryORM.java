package universite.angers.master.info.orm.bd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import universite.angers.master.info.orm.bd.annotation.Table;

/**
 * Classe qui permet de construire une base de données
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class FactoryORM<T extends SessionORM, K extends DataBaseORM> {

	/**
	 * Liste des tables
	 */
	protected List<Class<?>> objects;
	
	/**
	 * La session pour se connecter à la BD
	 */
	protected T session;
	
	/**
	 * Définition de la BD
	 */
	protected K dataBase;
	
	protected FactoryORM() {
		this.objects = new ArrayList<>();
	}

	/**
	 * Construction de la base de données
	 */
	public abstract void build();

	/**
	 * Ajout des tables par une liste de noms de paquetages
	 * @param packagesName
	 * @return
	 */
	public FactoryORM<T, K> addClassObject(String... packagesName) {
		for(String packageName : Arrays.asList(packagesName)) {
			this.addClassObject(packageName);
		}
		return this;
	}
	
	/**
	 * Ajout des tables par le nom du paquetage
	 * @param packageName
	 * @return
	 */
	public FactoryORM<T, K> addClassObject(String packageName) {
		Reflections reflections = new Reflections(packageName);
		Set<Class<?>> classAnnotatedTable = reflections.getTypesAnnotatedWith(Table.class);
		
		this.objects.addAll(classAnnotatedTable);
		return this;
	}
	
	/**
	 * Ajout des tables par une liste de class
	 * @param classObject
	 * @return
	 */
	public FactoryORM<T, K> addClassObject(Class<?>... classObject) {
		this.objects.addAll(Arrays.asList(classObject));
		return this;
	}
	
	/**
	 * Ajout des tables par une classe
	 * @param classObject
	 * @return
	 */
	public FactoryORM<T, K> addClassObject(Class<?> classObject) {
		this.objects.add(classObject);
		return this;
	}
	
	/**
	 * Ajout de la session
	 * @param session
	 * @return
	 */
	public FactoryORM<T, K> addSession(T session) {
		this.session = session;
		return this;
	}
	
	/**
	 * @return the objects
	 */
	public List<Class<?>> getObjects() {
		return objects;
	}

	/**
	 * @param objects the objects to set
	 */
	public void setObjects(List<Class<?>> objects) {
		this.objects = objects;
	}

	/**
	 * @return the session
	 */
	public T getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(T session) {
		this.session = session;
	}

	/**
	 * @return the dataBase
	 */
	public K getDataBase() {
		return dataBase;
	}

	/**
	 * @param dataBase the dataBase to set
	 */
	public void setDataBase(K dataBase) {
		this.dataBase = dataBase;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataBase == null) ? 0 : dataBase.hashCode());
		result = prime * result + ((objects == null) ? 0 : objects.hashCode());
		result = prime * result + ((session == null) ? 0 : session.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FactoryORM<?, ?> other = (FactoryORM<?, ?>) obj;
		if (dataBase == null) {
			if (other.dataBase != null)
				return false;
		} else if (!dataBase.equals(other.dataBase))
			return false;
		if (objects == null) {
			if (other.objects != null)
				return false;
		} else if (!objects.equals(other.objects))
			return false;
		if (session == null) {
			if (other.session != null)
				return false;
		} else if (!session.equals(other.session))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FactoryORM [objects=" + objects + ", session=" + session + ", dataBase=" + dataBase + "]";
	}
}
