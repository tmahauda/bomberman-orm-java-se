package universite.angers.master.info.orm.bd.column;

import java.lang.reflect.Field;

/**
 * Classe qui permet d'identifier les colonnes clés primaires
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class ColumnPrimaryKeyORM extends ColumnORM {

	public ColumnPrimaryKeyORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue)  {
		super(field, clazz, name, constraints, defaultValue);
	}
	
	public ColumnPrimaryKeyORM(Field field) {
		super(field);
	}
}
