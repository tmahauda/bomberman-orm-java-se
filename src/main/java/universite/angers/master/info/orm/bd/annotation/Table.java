package universite.angers.master.info.orm.bd.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation qui permet d'identifier une classe table pour la BD dans le moteur de réflection
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
@Retention(RUNTIME) //Paramétrer pour le moteur de réflection
@Target(TYPE) //Porter uniquement sur les classes
public @interface Table {
	boolean isAutoIncrement() default false;
	boolean isInstanciable() default true;
	String name() default "";
}
