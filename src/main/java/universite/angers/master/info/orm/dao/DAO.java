package universite.angers.master.info.orm.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.Accessiable;
import universite.angers.master.info.orm.bd.FactoryORM;

/**
 * Interface qui permet de communiquer avec une table d'une base de données
 * Cette DAO est accessible avec une API
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class DAO<T> implements Accessiable<T, Collection<T>> {

	/**
	 * Log de classe DAO
	 */
	private static final Logger LOG = Logger.getLogger(DAO.class);
	
	/**
	 * L'orm qui contient la BD
	 */
	private FactoryORM<?, ?> orm;
	
	/**
	 * Le type d'objet manipulé par la DAO = la table
	 */
	private Class<T> type;
	
	public DAO(FactoryORM<?, ?> orm, Class<T> type) {
		this.orm = orm;
		LOG.debug("ORM : " + this.orm);
		
		this.type = type;
		LOG.debug("Type : " + this.type);
	}
	
	@Override
	public synchronized boolean create(T object) {
		return this.orm.getDataBase().create(object);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public synchronized T read(String column, Object value) {
		Collection<T> objects = this.readAll(column, value);
		
		if(!objects.isEmpty())
			return (T)objects.toArray()[0];
		else
			return null;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public synchronized T read(Map<String, Object> where) {
		Collection<T> objects = this.readAll(where);
		
		if(!objects.isEmpty())
			return (T)objects.toArray()[0];
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public synchronized Collection<T> readAll() {
		Collection<T> read = new ArrayList<>();
		Collection<Object> objects = this.orm.getDataBase().read(this.type);
		
		for(Object o : objects) {
			read.add((T)o);
		}
		
		return read;
	}
	
	@Override
	public synchronized Collection<T> readAll(String column, Object value) {
		return this.readAll(Collections.singletonMap(column, value));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public synchronized Collection<T> readAll(Map<String, Object> where) {
		Collection<T> read = new ArrayList<>();
		Collection<Object> objects = this.orm.getDataBase().read(this.type, where);
		
		for(Object o : objects) {
			read.add((T)o);
		}
		
		return read;
	}
	
	@Override
	public synchronized boolean update(T object) {
		return this.orm.getDataBase().update(object);
	}
	
	@Override
	public synchronized boolean delete(T object) {
		return this.orm.getDataBase().delete(object);
	}
	
	/**
	 * @return the orm
	 */
	public FactoryORM<?, ?> getOrm() {
		return orm;
	}

	/**
	 * @param orm the orm to set
	 */
	public void setOrm(FactoryORM<?, ?> orm) {
		this.orm = orm;
	}

	/**
	 * @return the type
	 */
	public Class<T> getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Class<T> type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orm == null) ? 0 : orm.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		DAO other = (DAO) obj;
		if (orm == null) {
			if (other.orm != null)
				return false;
		} else if (!orm.equals(other.orm))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DAO [orm=" + orm + ", type=" + type + "]";
	}
}