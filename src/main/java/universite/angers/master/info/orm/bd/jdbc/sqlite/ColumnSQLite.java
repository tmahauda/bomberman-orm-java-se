package universite.angers.master.info.orm.bd.jdbc.sqlite;

import java.lang.reflect.Field;
import java.util.Arrays;
import universite.angers.master.info.orm.bd.column.ColumnORM;

/**
 * Classe colonne primitive SQLite
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ColumnSQLite extends ColumnORM {
	
	public ColumnSQLite(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue) {
		super(field, clazz, name, constraints, defaultValue);
	}
	
	public ColumnSQLite(Field field) {
		super(field);
	}
	
	@Override
	public String getTypeBoolean() {
		return "INTEGER";
	}
	
	@Override
	public String getTypeByte() {
		return "INTEGER";
	}
	
	@Override
	public String getTypeInteger() {
		return "INTEGER";
	}
	
	
	@Override
	public String getTypeShort() {
		return "REAL";
	}
	
	@Override
	public String getTypeLong() {
		return "REAL";
	}
	
	@Override
	public String getTypeFloat() {
		return "REAL";
	}
	
	@Override
	public String getTypeDouble() {
		return "REAL";
	}
	
	@Override
	public String getTypeCharacter() {
		return "TEXT";
	}
	
	@Override
	public String getTypeString() {
		return "TEXT";
	}
	
	@Override
	public String getTypeDate() {
		return "TEXT";
	}
	
	@Override
	public String executeCommandQueryCreateSQL() {
		if(this.constraints.length == 0) {
			return this.name + " " + this.type;
		} else {
			return this.name + " " + this.type + " " + String.join(" ", this.constraints);
		}
	}
	
	@Override
	public String executeCommandQueryDeleteSQL() {
		return "";
	}

	@Override
	public String toString() {
		return "ColumnSQLite [field=" + field + ", clazz=" + clazz + ", primitiveValue=" + primitiveValue
				+ ", defaultValue=" + defaultValue + ", collection=" + collection + ", array=" + array + ", name="
				+ name + ", type=" + type + ", constraints=" + Arrays.toString(constraints) + "]";
	}
}
