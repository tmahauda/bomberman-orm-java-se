package universite.angers.master.info.orm.bd;

/**
 * Interface qui permet d'effectuer une exécution d'une entité (colonne, table ou bd) dans la BD
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Executable {

	/**
	 * Commande requête pour créer une entité dans la BD
	 * (Colonne, table, bd, etc)
	 * @return
	 */
	public String executeCommandQueryCreateSQL();
	
	/**
	 * Commande requête pour supprimer supprimer une entité dans la BD
	 * (Colonne, table, bd, etc)
	 * @return
	 */
	public String executeCommandQueryDeleteSQL();
}
