package universite.angers.master.info.orm.bd.jdbc;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import universite.angers.master.info.orm.bd.SessionORM;
import universite.angers.master.info.orm.bd.WrapperType;
import universite.angers.master.info.orm.bd.column.ColumnForeignKeyORM;
import universite.angers.master.info.orm.bd.column.ColumnORM;
import universite.angers.master.info.orm.bd.column.ColumnPrimaryKeyORM;
import universite.angers.master.info.orm.bd.table.TableORM;
import universite.angers.master.info.orm.util.UtilORM;

/**
 * Classe qui permet de se connecter via JDBC
 * 
 *   Sources : 
 * - https://www.jmdoudoux.fr/java/dej/chap-jdbc.htm#jdbc-13
 * - http://blog.paumard.org/cours/jdbc/chap02-apercu-exemple.html
 * - https://www.codejava.net/java-se/jdbc/jdbc-database-connection-url-for-common-databases
 * - https://www.sqlitetutorial.net/sqlite-foreign-key/
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class SessionJdbcORM extends SessionORM {

	private static final Logger LOG = Logger.getLogger(SessionJdbcORM.class);
	
	/**
	 * La chaine de connection
	 */
	private String url;
	
	/**
	 * Les propriétés de connection
	 */
	private Properties properties;
	
	/**
	 * Driver de la BD afin de s'y connecter
	 */
	private Driver driver;
	
	/**
	 * La connection de JDBC
	 */
	private Connection connection;
	
	public SessionJdbcORM(boolean create, String name, String path, String provider, String host, String port) {
		this(create, name, path, provider, host, port, "", "");
	}
	
	public SessionJdbcORM(boolean create, String name, String path, String provider, String host, String port,
			String userName, String password) {
		super(create, name, path, provider, host, port, userName, password);
		
		this.driver = this.initDriver();
		LOG.debug("Driver : " + this.driver);
		
		this.url = this.initURL();
		LOG.debug("URL : " + this.url);
		
		this.properties = this.initProperties();
		LOG.debug("Propriétés : " + this.properties);
	}
	
	/**
	 * Récupérer le driver pour se connecter au serveur de base de données
	 * @return
	 */
	public abstract Driver initDriver();
	
	/**
	 * Récupérer la chaine de connection pour se connecter au serveur de base de données
	 * @return
	 */
	public abstract String initURL();
	
	/**
	 * Récupérer les paramétres de configurations pour se connecter au serveur de base de données
	 * @return
	 */
	public abstract Properties initProperties();
	
	@Override
	public boolean create(TableORM table) {
		LOG.debug("initialize table : " + table);
		
		if(table == null) return false;
		else {
			LOG.debug("initialize command create query : " + table.executeCommandQueryCreateSQL());
			return this.executeQuery(table.executeCommandQueryCreateSQL());
		}
	}
	
	@Override
	public boolean delete(TableORM table) {
		LOG.debug("initialize table : " + table);
		
		if(table == null) return false;
		else {
			LOG.debug("initialize command delete query : " + table.executeCommandQueryDeleteSQL());
			return this.executeQuery(table.executeCommandQueryDeleteSQL());
		}
	}
	
	@Override
	public boolean executeQuery(String commandSQL) {
		LOG.debug("executeQuery commande SQL : " + commandSQL);
		if(UtilORM.isNullOrEmpty(commandSQL)) return false;
		
		try {
			Statement stmt = this.connection.createStatement();
			if(stmt == null) return false;
			
			stmt.execute(commandSQL);
			stmt.close();
			
			return true;
		} catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}
	
	@Override
	public boolean open() {
		try {
			this.create();
			
			DriverManager.registerDriver(this.driver);
			LOG.debug("Enregistrement du driver : " + this.driver);
			
			this.connection = DriverManager.getConnection(this.url, this.properties);
			LOG.debug("Connexion à la base de données : " + this.connection);
			
			LOG.debug("Connexion fermé : " + this.connection.isClosed());
			return this.connection != null && !this.connection.isClosed();
			
		} catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean close() {
		try {
			DriverManager.deregisterDriver(this.driver);
			LOG.debug("Déconnexion du driver : " + this.driver);
			
			this.connection.close();
			LOG.debug("Fermeture de connection à la BD : " + this.connection);
			
			boolean isClose = this.connection.isClosed();
			this.connection = null;
			
			return this.connection == null && isClose;
			
		} catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}
	
	@Override
	public boolean executeCreate(TableORM table, Object object) {
		LOG.debug("executeCreate table insert: " + table);
		LOG.debug("executeCreate object insert : " + object);
		
		if(table == null) return false;
		if(object == null) return false;
		
		//Si l'objet est déjà enregistré dans la table dans ce cas return false
		//On évite un stackoverflow lors de la récursivité
		if(table.getInstances().contains(object)) return false;
		
		//On remonte jusqu'à la racine de la super table si présent
		//Puis on redescent du haut vers le bas pour récupérer l'ensembles des attributs
		if(table.getTableParent() != null)
			this.executeCreate(table.getTableParent(), object);
		
		try {
			//On récupére la commande d'insertion
			String createSQL = table.getCreateCommandSQL(table.getColumns().size() + table.getColumnsPrimaryKeys().size());
			LOG.debug("executeCreate command insert : " + createSQL);	
			
			if(UtilORM.isNullOrEmpty(createSQL)) return false;
			
			//On prépare une requete préparé qui va analyser la requete
			//Beaucoup plus puissant qu'une requete simple
			//On évite une injection SQL d'une commande malveillante
			PreparedStatement ps;
			
			if(table.isAutoIncrementValue()) {
				ps = this.connection.prepareStatement(createSQL, Statement.RETURN_GENERATED_KEYS);
				LOG.debug("executeCreate preparedStatement : " + ps);
			} else {
				ps = this.connection.prepareStatement(createSQL);
				LOG.debug("executeCreate preparedStatement : " + ps);
			}
			
			if(ps == null) return false;
			
			//Index pour placer les valeurs à chaque colonne
			//On commence à la colonne 1
			int parameterIndex = 1;
			
			//Pour chaque colonne primitives
			for(ColumnORM column : table.getColumns().values()) {
				LOG.debug("executeCreate column primitive insert : " + column);
				if(column == null) continue;
				
				Object value = column.getValue(object);
				LOG.debug("executeCreate value primitive insert " + value);
				
				ps.setObject(parameterIndex, value);
				parameterIndex++;
			}
			
			//Si l'ID est géré automatiquement
			if(table.isAutoIncrementValue()) {
				ps.setObject(parameterIndex, null);
				parameterIndex++;
			} else {
				//Pour chaque clé primaire
				for(ColumnORM column : table.getColumnsPrimaryKeys().values()) {
					LOG.debug("executeCreate column primary key insert : " + column);
					if(column == null) continue;
					
					Object value = column.getValue(object);
					LOG.debug("executeCreate value primary key insert " + value);
					
					ps.setObject(parameterIndex, value);
					parameterIndex++;
				}	
			}
			
			//On enregistre le tout
			ps.executeUpdate();
			
			//On récupère la clé généré et on l'affecte à l'objet
			if(table.isAutoIncrementValue()) {
				ResultSet rs = ps.getGeneratedKeys();
				if(rs == null) return false;
				
				while(rs.next()) {
					//On ne peut avoir qu'une seule clé primaire auto-généré
					ColumnORM column = (ColumnORM)table.getColumnsPrimaryKeys().values().toArray()[0];
					
					int key = rs.getInt(1);
					LOG.debug("executeCreate key auto-generated " + key);
					
					column.setValue(object, key);
				}
			}
			
			ps.close();	
			table.getInstances().add(object);
			LOG.debug("executeCreate object enregistré dans la table : " + object);
			
			//On s'occupe des sous-objets complexes
			
			//Pour chaque clés étrangères
			for(ColumnForeignKeyORM columnForeign : table.getColumnsForeignKeys().values()) {
				LOG.debug("executeCreate column foreign key insert : " + columnForeign);
				if(columnForeign == null) continue;
				
				//Si la colonne nous autorise à faire des create en cascade
				LOG.debug("executeCreate on create cascade : " + columnForeign.isOnCreateCascade());
				if(!columnForeign.isOnCreateCascade()) continue;
				
				//On récupère la table étrangère
				TableORM tableForeign = columnForeign.getTableTarget();
				LOG.debug("executeCreate table foreign : " + tableForeign);
				if(tableForeign == null) continue;
				
				//Si cette colonne possède un champ collection d'objets
				if(columnForeign.isCollection()) {
					@SuppressWarnings("unchecked")
					Collection<Object> objectsForeign = (Collection<Object>)columnForeign.getValue(object);
					LOG.debug("executeCreate collection objets foreign : " + objectsForeign);
					
					if(objectsForeign == null) continue;
					
					for(Object objectForeign : objectsForeign) {
						LOG.debug("executeCreate objet foreign : " + objectForeign);
						if(objectForeign == null) continue;
						
						boolean record = this.executeCreateObjectTargetAndLink(columnForeign, object, objectForeign);
						LOG.debug("executeCreate record objet foreign and source : " + record);
					}
				//Possède d'un tableau d'objets
				} else if(columnForeign.isArray()) {
					Object o = columnForeign.getValue(object);
					LOG.debug("executeCreate array primitive objets foreign " + o);
					
					if(o == null) continue;
					
					Object[] objectsForeign = (Object[])WrapperType.convertArrayPrimitiveToArrayWrapper(o);
					LOG.debug("executeCreate array wrapped objets foreign : " + objectsForeign);
					
					if(objectsForeign == null) continue;

					for(Object objectForeign : objectsForeign) {
						LOG.debug("executeCreate objet foreign : " + objectForeign);
						if(objectForeign == null) continue;
						
						boolean record = this.executeCreateObjectTargetAndLink(columnForeign, object, objectForeign);
						LOG.debug("executeCreate record objet foreign and source : " + record);
					}
				//Possède un seul objet
				} else {
					//On récupère l'objet
					Object objectForeign = columnForeign.getValue(object);
					LOG.debug("executeCreate objet foreign : " + objectForeign);
					if(objectForeign == null) continue;
					
					boolean record = this.executeCreateObjectTargetAndLink(columnForeign, object, objectForeign);
					LOG.debug("executeCreate record objet foreign and source : " + record);
				}
			}
			
			return true;
		} 
		catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		} 
	}
	
	/**
	 * Enregistre l'objet étranger dans sa table et le lien entre un objet source et target
	 * dans la table association
	 * @param columnForeign
	 * @param objectSource
	 * @param objectTarget
	 * @return
	 */
	private boolean executeCreateObjectTargetAndLink(ColumnForeignKeyORM columnForeign, Object objectSource, Object objectTarget) {
		LOG.debug("executeCreateObjectTargetAndLink column foreign : " + columnForeign);
		LOG.debug("executeCreateObjectTargetAndLink object source : " + objectSource);
		LOG.debug("executeCreateObjectTargetAndLink object target : " + objectTarget);
		
		if(columnForeign == null) return false;
		if(objectSource == null) return false;
		if(objectTarget == null) return false;
		
		boolean createObjectForeign = false;
		if(!columnForeign.isPrimitiveValue()) {
			
			//On récupère la table correspondant à l'objet target si présent
			String classTarget = objectTarget.getClass().getName();
			LOG.debug("executeCreateObjectTargetAndLink class target : " + classTarget);
			
			if(!this.dataBase.getTables().containsKey(classTarget)) return false;
			TableORM tableTarget = this.dataBase.getTables().get(classTarget);
			LOG.debug("executeCreateObjectTargetAndLink table target : " + tableTarget);
			
			if(tableTarget == null) return false;
			
			//Puis on enregistre l'objet étranger dans la table correspondante par récusirvité
			createObjectForeign = this.executeCreate(tableTarget, objectTarget);
			LOG.debug("executeCreateObjectTargetAndLink create object foreign : " + createObjectForeign);	
		} 
		else createObjectForeign = true;
		
		//Puis le lien entre l'objet courant et l'objet étranger
		boolean createLink = this.executeCreateInTableAssociation(columnForeign, objectSource, objectTarget);
		LOG.debug("executeCreateObjectTargetAndLink link object and object foreign in table association : " + createLink);
		
		return createObjectForeign && createLink;
	}

	/**
	 * Méthode qui permet d'insérer dans la table association un lien entre un objet source et target
	 * @param columnForeign
	 * @param objectSource
	 * @param objectTarget
	 * @return
	 */
	private boolean executeCreateInTableAssociation(ColumnForeignKeyORM columnForeign, Object objectSource, Object objectTarget) {
		LOG.debug("executeCreateInTableAssociation colonnes foreign : " + columnForeign);
		LOG.debug("executeCreateInTableAssociation object source : " + objectSource);
		LOG.debug("executeCreateInTableAssociation object target : " + objectTarget);
		
		if(columnForeign == null) return false;
		if(objectSource == null) return false;
		if(objectTarget == null) return false;
		
		try {
			//On récupère le nombre de points d'interrogation à placé pour la requete préparé
			
			int sizeSource = columnForeign.getTableSource().getColumnsPrimaryKeys().size();
			LOG.debug("executeCreateInTableAssociation size source : " + sizeSource);
			
			int sizeTarget = columnForeign.getTableTarget().getColumnsPrimaryKeys().size();
			LOG.debug("executeCreateInTableAssociation size target : " + sizeTarget);
			
			//Le nombre de points ? à placer
			int size = sizeSource + sizeTarget;
			LOG.debug("executeCreateInTableAssociation size : " + size);
			
			String commandSQLCreate = columnForeign.getTableAssociation().getCreateCommandSQL(size);
			LOG.debug("executeCreateInTableAssociation command SQL create : " + commandSQLCreate);
			
			if(UtilORM.isNullOrEmpty(commandSQLCreate)) return false;
			
			PreparedStatement ps = this.connection.prepareStatement(commandSQLCreate);
			LOG.debug("executeCreateInTableAssociation preparedStatement : " + ps);
			
			if(ps == null) return false;
			
			int parameterIndex = 1;
			
			//Pour toutes les clés primaires de la première table
			for(ColumnORM column : columnForeign.getTableSource().getColumnsPrimaryKeys().values()) {
				LOG.debug("executeCreateInTableAssociation column primary key table source insert : " + column);
				if(column == null) continue;
				
				//Attention ! la valeur peut etre null
				Object value = column.getValue(objectSource);
				LOG.debug("executeCreateInTableAssociation value primary key table source insert " + value);
				
				ps.setObject(parameterIndex, value);
				parameterIndex++;
			}
			
			if(columnForeign.isPrimitiveValue()) {
				ps.setObject(parameterIndex, objectTarget);
				parameterIndex++;
			} else {
				//Pour toutes les clés primaires de la seconde table
				for(ColumnORM column : columnForeign.getTableTarget().getColumnsPrimaryKeys().values()) {
					LOG.debug("executeCreateInTableAssociation column primary key table target insert : " + column);
					if(column == null) continue;
					
					//Attention ! la valeur peut etre null
					Object value = column.getValue(objectTarget);
					LOG.debug("executeCreateInTableAssociation value primary key table target insert " + value);
					
					ps.setObject(parameterIndex, value);
					parameterIndex++;
				}	
			}
						
			//Le lien est enregistré
			ps.executeUpdate();
			ps.close();
			
			return true;
		} catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		} 
	}
	
	@Override
	public boolean executeUpdate(TableORM table, Object object) {
		LOG.debug("executeUpdate table insert: " + table);
		LOG.debug("executeUpdate object insert : " + object);
		
		if(table == null) return false;
		if(object == null) return false;
		
		//Si l'objet est déjà enregistré dans la table dans ce cas return false
		if(table.getInstances().contains(object)) return false;
		
		//On remonte jusqu'à la racine de la super table si présent
		if(table.getTableParent() != null)
			this.executeUpdate(table.getTableParent(), object);
		
		try {
			
			//Si il n'y a pas de colonnes primitives à mettre à jour dans ce cas pas de maj de la table
			LOG.debug("executeUpdate size columns primitives : " + table.getColumns().size());
			
			if(!table.getColumns().isEmpty()) {
				String updateSQL = table.getUpdateCommandSQL(table.getColumnsPrimaryKeys());
				LOG.debug("executeUpdate command update : " + updateSQL);
				
				if(UtilORM.isNullOrEmpty(updateSQL)) return false;
				
				PreparedStatement ps = this.connection.prepareStatement(updateSQL);
				LOG.debug("executeUpdate preparedStatement : " + ps);
				
				if(ps == null) return false;
				
				int parameterIndex = 1;
				
				//Pour chaque colonne primitives à modifier
				for(ColumnORM column : table.getColumns().values()) {
					LOG.debug("executeUpdate column primitive update : " + column);
					if(column == null) continue;
					
					Object value = column.getValue(object);
					LOG.debug("executeUpdate value primitive update " + value);
					
					ps.setObject(parameterIndex, value);
					parameterIndex++;
				}

				//Pour chaque clé primaire dans la clause where
				for(ColumnORM column : table.getColumnsPrimaryKeys().values()) {
					LOG.debug("executeUpdate column primary key update : " + column);
					if(column == null) continue;
					
					Object value = column.getValue(object);
					LOG.debug("executeUpdate value primary key update " + value);
					ps.setObject(parameterIndex, value);
					parameterIndex++;
				}

				ps.executeUpdate();
				ps.close();
			}
			
			table.getInstances().add(object);
			
			//Puis pour chaque clés étrangères
			for(ColumnForeignKeyORM columnForeign : table.getColumnsForeignKeys().values()) {
				LOG.debug("executeUpdate column foreign key update : " + columnForeign);
				if(columnForeign == null) continue;
				
				//Si la colonne nous autorise à faire des maj en cascade
				LOG.debug("executeUpdate on update cascade : " + columnForeign.isOnUpdateCascade());
				if(!columnForeign.isOnUpdateCascade()) continue;
				
				//On récupère la table étrangère
				TableORM tableForeign = columnForeign.getTableTarget();
				LOG.debug("executeUpdate table foreign : " + tableForeign);
				
				if(tableForeign == null) continue;
				
				//Si cette colonne est issu d'un champ collection
				if(columnForeign.isCollection()) {
					@SuppressWarnings("unchecked")
					Collection<Object> objectsForeign = (Collection<Object>)columnForeign.getValue(object);
					LOG.debug("executeUpdate collection objets update : " + objectsForeign);
					
					//Ici il faut éventuellement supprimer les liens
					//Pour les valeurs primitives on supprime toutes les anciennes valeurs
					//Puis on ajoute les nouvelles valeurs dans la boucle
					//En effet on ne peut pas savoir quelles sont les valeurs primitives qui ont été modifiés
					if(objectsForeign == null || objectsForeign.isEmpty() || columnForeign.isPrimitiveValue()) {
						boolean delete = this.executeDeleteInTableAssociation(columnForeign, object);
						LOG.debug("executeUpdate delete in table assoc : " + delete);
					}
					
					if(objectsForeign == null) continue;
					
					for(Object objectForeign : objectsForeign) {
						LOG.debug("executeUpdate objet foreign : " + objectForeign);
						if(objectForeign == null) continue;
						
						boolean record = this.executeUpdateObjectTargetAndLink(columnForeign, object, objectForeign);
						LOG.debug("executeUpdate record objet foreign and source : " + record);
					}
					
				} else if(columnForeign.isArray()) {
					Object o = columnForeign.getValue(object);
					LOG.debug("executeUpdate array primitive objets foreign " + o);
					
					if(o == null) continue;
					
					Object[] objectsForeign = (Object[])WrapperType.convertArrayPrimitiveToArrayWrapper(o);

					LOG.debug("executeUpdate array wrapped objets update : " + objectsForeign);
					
					//On supprime toutes les anciennes valeurs
					//Puis on ajoute les nouvelles valeurs dans la boucle
					//En effet on ne peut pas savoir quelles sont les valeurs primitives qui ont été modifiés
					if(objectsForeign == null || objectsForeign.length == 0 || columnForeign.isPrimitiveValue()) {
						boolean delete = this.executeDeleteInTableAssociation(columnForeign, object);
						LOG.debug("executeUpdate delete in table assoc : " + delete);
					}
					
					if(objectsForeign == null) continue;
					
					for(Object objectForeign : objectsForeign) {
						LOG.debug("executeUpdate objet foreign : " + objectForeign);
						if(objectForeign == null) continue;
						
						boolean record = this.executeUpdateObjectTargetAndLink(columnForeign, object, objectForeign);
						LOG.debug("executeUpdate record objet foreign and source : " + record);
					}
				} else {
					//On récupère l'objet
					Object objectForeign = columnForeign.getValue(object);
					LOG.debug("executeUpdate objet foreign : " + objectForeign);
					
					//On supprime toutes les anciennes valeurs
					//Puis on ajoute les nouvelles valeurs dans la boucle
					//En effet on ne peut pas savoir quelles sont les valeurs primitives qui ont été modifiés
					if(objectForeign == null || columnForeign.isPrimitiveValue()) {
						boolean delete = this.executeDeleteInTableAssociation(columnForeign, object);
						LOG.debug("executeUpdate delete in table assoc : " + delete);
					}
					
					if(objectForeign == null) continue;
					
					boolean record = this.executeUpdateObjectTargetAndLink(columnForeign, object, objectForeign);
					LOG.debug("executeUpdate record objet foreign and source : " + record);
				}
			}
			
			return true;
		} 
		catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		} 
	}
	
	/**
	 * Supprimer un objet dans une table association
	 * @param columnForeign
	 * @param object
	 * @return
	 */
	private boolean executeDeleteInTableAssociation(ColumnForeignKeyORM columnForeign, Object object) {
		LOG.debug("executeDeleteInTableAssociation column foreign : " + columnForeign);
		LOG.debug("executeDeleteInTableAssociation object : " + object);
		
		if(columnForeign == null) return false;
		if(object == null) return false;
		
		try {
			TableORM tableAsso = columnForeign.getTableAssociation();
			LOG.debug("executeDeleteInTableAssociation table assoc : " + tableAsso);
			
			Map<String, ColumnPrimaryKeyORM> columnsWhere = columnForeign.getTableAssociation().getColumnsPrimaryKeys();
			LOG.debug("executeDeleteInTableAssociation columns where : " + columnsWhere);
			
			//On prépare la requete pour supprimer dans la table association
			String sqlCommandDelete = tableAsso.getDeleteCommandSQL(columnsWhere);
			LOG.debug("executeDeleteInTableAssociation command SQL delete : " + sqlCommandDelete);
			
			PreparedStatement ps = this.connection.prepareStatement(sqlCommandDelete);
			LOG.debug("executeDeleteInTableAssociation preparedStatement : " + ps);

			if(ps == null) return false;
			
			int parameterIndex = 1;
			
			//Pour chaque clé primaire
			for(ColumnORM column : tableAsso.getColumnsPrimaryKeys().values()) {
				LOG.debug("executeDeleteInTableAssociation column primary key insert : " + column);
				if(column == null) continue;
				
				Object value = column.getValue(object);
				LOG.debug("executeDeleteInTableAssociation value primary key insert " + value);
				
				ps.setObject(parameterIndex, value);
				parameterIndex++;
			}
			
			//On supprime l'objet
			//Les liens référencés dans les autres tables seront supprimés en cascade
			ps.executeUpdate();
			ps.close();
			
			return true;
			
		} catch(SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}
	
	/**
	 * Enregistre l'objet étranger dans sa table et le lien entre un objet source et target
	 * dans la table association
	 * @param columnForeign
	 * @param objectSource
	 * @param objectTarget
	 * @return
	 */
	private boolean executeUpdateObjectTargetAndLink(ColumnForeignKeyORM columnForeign, Object objectSource, Object objectTarget) {
		LOG.debug("executeUpdateObjectTargetAndLink column foreign : " + columnForeign);
		LOG.debug("executeUpdateObjectTargetAndLink object source : " + objectSource);
		LOG.debug("executeUpdateObjectTargetAndLink object target : " + objectTarget);
		
		if(columnForeign == null) return false;
		if(objectSource == null) return false;
		if(objectTarget == null) return false;
		
		boolean updateObjectForeign = false;
		if(!columnForeign.isPrimitiveValue()) {
			
			//On récupère la table correspondant à l'objet target si présent
			String classTarget = objectTarget.getClass().getName();
			LOG.debug("executeUpdateObjectTargetAndLink class target : " + classTarget);
			
			if(!this.dataBase.getTables().containsKey(classTarget)) return false;
			TableORM tableTarget = this.dataBase.getTables().get(classTarget);
			LOG.debug("executeUpdateObjectTargetAndLink table target : " + tableTarget);
			
			if(tableTarget == null) return false;
			
			//On crée l'objet et le lien si non déjà fait
			boolean createObjectForeign = this.executeCreateObjectTargetAndLink(columnForeign, objectSource, objectTarget);
			LOG.debug("executeCreateObjectTargetAndLink create object foreign : " + createObjectForeign);
			
			//Puis on maj l'objet étranger dans la table correspondante par récursirvité
			updateObjectForeign = this.executeUpdate(tableTarget, objectTarget);
			LOG.debug("executeUpdateObjectTargetAndLink update object foreign : " + updateObjectForeign);
		} 
		else {
			//On ajoute les nouvelles valeurs dans la table association
			updateObjectForeign = this.executeCreateInTableAssociation(columnForeign, objectSource, objectTarget);
			LOG.debug("executeUpdateObjectTargetAndLink update object foreign : " + updateObjectForeign);
		}
		
		return updateObjectForeign;
	}
	
	@Override
	public boolean executeDelete(TableORM table, Object object) {
		LOG.debug("executeDelete table insert: " + table);
		LOG.debug("executeDelete object insert : " + object);
		
		if(table == null) return false;
		if(object == null) return false;
		
		//Si l'objet est déjà supprimé dans la table dans ce cas return false
		//On évite un stackoverflow lors de la récursivité
		if(table.getInstances().contains(object)) return false;
		
		//On remonte jusqu'à la racine de la super table si présent
		//Puis on redescent du haut vers le bas pour supprimer l'ensembles des attributs
		if(table.getTableParent() != null)
			this.executeDelete(table.getTableParent(), object);

		try {
			String deleteSQL = table.getDeleteCommandSQL(table.getColumnsPrimaryKeys());
			LOG.debug("executeDelete command delete : " + deleteSQL);
			
			PreparedStatement ps = this.connection.prepareStatement(deleteSQL);
			LOG.debug("executeDelete preparedStatement : " + ps);
			
			if(ps == null) return false;
			
			int parameterIndex = 1;
			
			//Pour chaque clé primaire
			for(ColumnORM column : table.getColumnsPrimaryKeys().values()) {
				LOG.debug("executeDelete column primary key insert : " + column);
				if(column == null) continue;
				
				Object value = column.getValue(object);
				LOG.debug("executeDelete value primary key insert " + value);
				
				ps.setObject(parameterIndex, value);
				parameterIndex++;
			}
			
			//On supprime l'objet
			//Les liens référencés dans les autres tables seront supprimés en cascade
			ps.executeUpdate();
			ps.close();
			
			return true;
		} 
		catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return false;
		} 
	}
	
	@Override
	public Collection<Object> executeRead(TableORM table, Map<String, ? extends ColumnORM> columnsSelect, 
			Map<? extends ColumnORM, Object> columnsWhere) {
		Collection<Object> objects = new ArrayList<>();
		
		LOG.debug("executeRead table : " + table);
		LOG.debug("executeRead columns select : " + columnsSelect);
		LOG.debug("executeRead columns where : " + columnsWhere);
		
		if(table == null) return objects;
		
		//Si la table ne peut pas être instancié
		LOG.debug("executeRead table instanciable : " + table.isInstanciableObject());
		if(!table.isInstanciableObject()) {
			//On récupère tout ce qu'il y a dans les sous-tables qui sont instanciables
			for(TableORM children : table.getTablesChildrens().values()) {
				LOG.debug("executeRead table children : " + children);
				 objects.addAll(this.executeRead(children, columnsSelect, columnsWhere));
			}
			
			return objects;
		}
		
		try {
			int parameterIndex = 1;
			String commandReadSQL = "";
			
			//On récupère la requete SQL selon le choix opté
			// - Toutes les colonnes sans condition
			// - Toutes les colonnes avec condition
			// - Certaines colonnes avec condition
			if(columnsSelect == null && columnsWhere == null) {
				commandReadSQL = table.getReadCommandSQL();
			} else if(columnsSelect == null && columnsWhere != null) {
				commandReadSQL = table.getReadCommandSQL(columnsWhere);
			} else {
				commandReadSQL = table.getReadCommandSQL(columnsSelect, columnsWhere);
			}
			
			LOG.debug("executeRead command read SQL : " + commandReadSQL);
			if(UtilORM.isNullOrEmpty(commandReadSQL)) return objects;
			
			PreparedStatement ps = this.connection.prepareStatement(commandReadSQL);
			LOG.debug("executeRead preparedStatement : " + ps);
			
			if(ps == null) return objects;
			
			//Si la requete comporte des conditions
			//dans ce cas on place les valeurs dans la requete préparé
			if(columnsWhere != null) {
				
				for(ColumnORM column : columnsWhere.keySet()) {
					
					Object valueSimple = columnsWhere.get(column);
					LOG.debug("executeRead value read object simple : " + valueSimple);
					
					//Si la valeur est un objet dans ce cas on récupère
					//les valeurs des clés primaires
					if(column instanceof ColumnForeignKeyORM) {
						
						ColumnForeignKeyORM columnForeign = (ColumnForeignKeyORM)column;
						TableORM tableTarget = columnForeign.getTableTarget();
						
						for(ColumnPrimaryKeyORM columnPK : tableTarget.getColumnsPrimaryKeys().values()) {
							Object valueComplex = columnPK.getValue(valueSimple);
							LOG.debug("executeRead value read object complex : " + valueComplex);
							
							ps.setObject(parameterIndex, valueComplex);
							parameterIndex++;
						}
					} else {
						ps.setObject(parameterIndex, valueSimple);
						parameterIndex++;
					}
				}	
			}
			
			ResultSet result = ps.executeQuery();
			LOG.debug("executeRead resultSet : " + result);
			
			if(result == null) return objects;
			
			//Pour chaque ligne
			while(result.next()) {
				
				//On récupère les méta-données de la ligne courante
				ResultSetMetaData rsmd = result.getMetaData();
				LOG.debug("executeRead meta data : " + rsmd);
				
				if(rsmd == null) continue;
				
				//On crée l'objet
				Object objectORM = table.getNewObject();
				LOG.debug("executeRead object ORM : " + objectORM);
				
				if(objectORM == null) continue;
				
				//Condition a ajouter pour récupérer éventuellement
				//des valeurs dans les super tables
				Map<ColumnORM, Object> where = new HashMap<>();
				
				//Injection des données dans l'objet ORM
				//Pour chaque champ de la ligne courante
				//Attention ! on commence à 1 et non à 0
				for(int i=1; i<=rsmd.getColumnCount(); i++) {
					String columnName = rsmd.getColumnName(i);
					LOG.debug("executeRead column name : " + columnName);

					if(UtilORM.isNullOrEmpty(columnName)) continue;
					
					if(table.getColumns().containsKey(columnName)) {
						//On récupère la colonne associé
						ColumnORM columnORM = table.getColumns().get(columnName);
						LOG.debug("executeRead column other ORM : " + columnORM);
						
						//Puis sa valeur
						Object valueORM = result.getObject(i);
						LOG.debug("executeRead value ORM : " + valueORM);	
						
						//On injecte la valeur dans l'instance
						columnORM.setValue(objectORM, valueORM);
					} 
					else if(table.getColumnsPrimaryKeys().containsKey(columnName)) {
						//On récupère la colonne associé
						ColumnORM columnORM = table.getColumnsPrimaryKeys().get(columnName);
						LOG.debug("executeRead column primary key ORM : " + columnORM);
						
						//Puis sa valeur
						Object valueORM = result.getObject(i);
						LOG.debug("executeRead value ORM : " + valueORM);	
						
						//On injecte la valeur dans l'instance
						columnORM.setValue(objectORM, valueORM);
						
						//On ajoute la valeur associé a la colonne
						//pour interogger des super tables
						where.put(columnORM, valueORM);
					}
				}
				
				LOG.debug("executeRead object ORM : " + objectORM);
				LOG.debug("executeRead columns where : " + where);
				
				//Si il y a des données a récupérer dans les super tables dans ce cas on le fait
				if(table.getTableParent() != null) {
					this.executeReadForValue(table.getTableParent(), null, where, objectORM);	
				}
				
				//On s'occupe des objets complexes avec les clés étrangères
				for(ColumnForeignKeyORM columnForeignKeyORM : table.getColumnsForeignKeys().values()) {
					LOG.debug("executeRead column foreign key ORM : " + columnForeignKeyORM);
					
					if(columnForeignKeyORM == null) continue;
					
					//Si la colonne nous autorise à faire des read en cascade
					LOG.debug("executeRead on read cascade : " + columnForeignKeyORM.isOnReadCascade());
					if(!columnForeignKeyORM.isOnReadCascade()) continue;
					
					//Si cette colonne est issu d'un champ collection
					if(columnForeignKeyORM.isCollection()) {
						Collection<Object> objectsForeignList = this.executeReadInTableAssociation(columnForeignKeyORM, objectORM); 
						LOG.debug("executeRead list objects foreign : " + objectsForeignList);

						@SuppressWarnings("unchecked")
						Collection<Object> objectsForeignCollection = (Collection<Object>)columnForeignKeyORM.getValue(objectORM);
						objectsForeignCollection.addAll(objectsForeignList);
						LOG.debug("executeRead collection objects foreign : " + objectsForeignCollection);
						
						columnForeignKeyORM.setValue(objectORM, objectsForeignCollection);
					} 
					else if(columnForeignKeyORM.isArray()) {
						Collection<Object> objectsForeignList = this.executeReadInTableAssociation(columnForeignKeyORM, objectORM); 
						LOG.debug("executeRead list objects foreign : " + objectsForeignList);
						
						Object o = Array.newInstance(columnForeignKeyORM.getClazz(), objectsForeignList.size());
						LOG.debug("executeRead array primitive objets foreign " + o);
						
						if(o == null) continue;
						
						Object[] objectsForeignArray = (Object[])WrapperType.convertArrayPrimitiveToArrayWrapper(o);
						LOG.debug("executeCreate array wrapped objets foreign : " + objectsForeignArray);
						
						if(objectsForeignArray == null) continue;
						
						int i = 0;
						for(Object objectForeign : objectsForeignList) {
							objectsForeignArray[i] = objectForeign;
							i++;
						}
						LOG.debug("executeRead array objects foreign : " + objectsForeignArray);
						
						columnForeignKeyORM.setValue(objectORM, objectsForeignArray);
					}
					else {
						Collection<Object> objectsForeignList = this.executeReadInTableAssociation(columnForeignKeyORM, objectORM); 
						LOG.debug("executeRead list objects foreign : " + objectsForeignList);
						
						Object objectForeign = null;
						
						if(!objectsForeignList.isEmpty()) {
							objectForeign = objectsForeignList.toArray()[0];
						}
						
						LOG.debug("executeRead object foreign : " + objectForeign);
						columnForeignKeyORM.setValue(objectORM, objectForeign);	
					}
				}
				
				//On ajoute l'objet dans la liste
				objects.add(objectORM);
			}
			
			result.close();
			ps.close();
			
			return objects;
			
		} catch(SQLException e) {
			LOG.error(e.getMessage(), e);
			return objects;
		}
	}
	
	/**
	 * Méthode qui permet de récupérer des valeurs dans des super tables pour un object
	 * @param table
	 * @param columnsSelect
	 * @param columnsWhere
	 * @param object
	 */
	private void executeReadForValue(TableORM table, Map<String, ? extends ColumnORM> columnsSelect, 
			Map<? extends ColumnORM, Object> columnsWhere, Object object) {
		
		LOG.debug("executeReadForValue table : " + table);
		LOG.debug("executeReadForValue columns select : " + columnsSelect);
		LOG.debug("executeReadForValue columns where : " + columnsWhere);
		
		if(table == null) return;
		
		try {
			int parameterIndex = 1;
			String commandReadSQL = "";
			
			if(columnsSelect == null && columnsWhere == null) {
				commandReadSQL = table.getReadCommandSQL();
			} else if(columnsSelect == null && columnsWhere != null) {
				commandReadSQL = table.getReadCommandSQL(columnsWhere);
			} else {
				commandReadSQL = table.getReadCommandSQL(columnsSelect, columnsWhere);
			}
			
			LOG.debug("executeReadForValue command read SQL : " + commandReadSQL);
			
			if(UtilORM.isNullOrEmpty(commandReadSQL)) return;
			
			PreparedStatement ps = this.connection.prepareStatement(commandReadSQL);
			LOG.debug("executeReadForValue preparedStatement : " + ps);
			
			if(ps == null) return;
			
			if(columnsWhere != null) {
				for(Object value : columnsWhere.values()) {
					LOG.debug("executeReadForValue value read : " + value);
					
					ps.setObject(parameterIndex, value);
					parameterIndex++;
				}	
			}
			
			ResultSet result = ps.executeQuery();
			LOG.debug("executeReadForValue resultSet : " + result);
			
			if(result == null) return;
			
			//Pour chaque ligne
			while(result.next()) {
				
				//On récupère les méta-données de la ligne courante
				ResultSetMetaData rsmd = result.getMetaData();
				LOG.debug("executeReadForValue meta data : " + rsmd);
	
				if(rsmd == null) continue;
				
				//Injection des données dans l'objet ORM
				//Pour chaque champ de la ligne courante
				//Attention ! on commence à 1 et non à 0
				for(int i=1; i<=rsmd.getColumnCount(); i++) {
					String columnName = rsmd.getColumnName(i);
					LOG.debug("executeReadForValue column name : " + columnName);

					if(UtilORM.isNullOrEmpty(columnName)) continue;
					
					if(table.getColumns().containsKey(columnName)) {
						//On récupère la colonne associé
						ColumnORM columnORM = table.getColumns().get(columnName);
						LOG.debug("executeReadForValue column other ORM : " + columnORM);
						
						//Puis sa valeur
						Object valueORM = result.getObject(i);
						LOG.debug("executeReadForValue value ORM : " + valueORM);	
						
						//On injecte la valeur dans l'instance
						columnORM.setValue(object, valueORM);
					} 
					else if(table.getColumnsPrimaryKeys().containsKey(columnName)) {
						//On récupère la colonne associé
						ColumnORM columnORM = table.getColumnsPrimaryKeys().get(columnName);
						LOG.debug("executeReadForValue column primary key ORM : " + columnORM);
						
						//Puis sa valeur
						Object valueORM = result.getObject(i);
						LOG.debug("executeReadForValue value ORM : " + valueORM);	
						
						//On injecte la valeur dans l'instance
						columnORM.setValue(object, valueORM);
					}
				}
				
				LOG.debug("executeReadForValue object ORM : " + object);
				
				//On s'occupe des objets complexes avec les clés étrangères
				for(ColumnForeignKeyORM columnForeignKeyORM : table.getColumnsForeignKeys().values()) {
					LOG.debug("executeReadForValue column foreign key ORM : " + columnForeignKeyORM);
					
					//Si cette colonne est issu d'un champ collection
					if(columnForeignKeyORM.isCollection()) {
						Collection<Object> objectsForeignList = this.executeReadInTableAssociation(columnForeignKeyORM, object); 
						LOG.debug("executeReadForValue list Objects foreign : " + objectsForeignList);

						@SuppressWarnings("unchecked")
						Collection<Object> objectsForeignCollection = (Collection<Object>)columnForeignKeyORM.getValue(object);
						objectsForeignCollection.addAll(objectsForeignList);
						
						LOG.debug("executeReadForValue collection Objects foreign : " + objectsForeignCollection);
						columnForeignKeyORM.setValue(object, objectsForeignCollection);
					} 
					else if(columnForeignKeyORM.isArray()) {
						Collection<Object> objectsForeignList = this.executeReadInTableAssociation(columnForeignKeyORM, object); 
						LOG.debug("executeReadForValue list Objects foreign : " + objectsForeignList);
						
						Object o = Array.newInstance(columnForeignKeyORM.getClazz(), objectsForeignList.size());
						LOG.debug("executeReadForValue array primitive objets foreign " + o);
						
						if(o == null) continue;
						
						Object[] objectsForeignArray = (Object[])WrapperType.convertArrayPrimitiveToArrayWrapper(o);
						LOG.debug("executeReadForValue array wrapped objets foreign : " + objectsForeignArray);
						
						int i = 0;
						for(Object objectForeign : objectsForeignList) {
							objectsForeignArray[i] = objectForeign;
							i++;
						}
						
						LOG.debug("executeReadForValue array Objects foreign : " + objectsForeignArray);
						columnForeignKeyORM.setValue(object, objectsForeignArray);
					}
					else {
						Collection<Object> objectsForeignList = this.executeReadInTableAssociation(columnForeignKeyORM, object); 
						LOG.debug("executeReadForValue list Objects foreign : " + objectsForeignList);
						
						Object objectForeign = null;
						
						if(!objectsForeignList.isEmpty()) {
							objectForeign = objectsForeignList.toArray()[0];
						}
						
						LOG.debug("executeReadForValue object foreign : " + objectForeign);
						columnForeignKeyORM.setValue(object, objectForeign);	
					}
				}
			}
			
			result.close();
			ps.close();
			
			//Si il y a encore des données dans la super table
			//on rappelle récursivement la méthode
			if(table.getTableParent() != null) {
				this.executeReadForValue(table.getTableParent(), columnsSelect, columnsWhere, object);
			}
			
		} catch(SQLException e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Méthode qui permet de récupérer une collection d'objet dans une table association
	 * @param columnForeignKeyORM
	 * @param object
	 * @return
	 */
	private Collection<Object> executeReadInTableAssociation(ColumnForeignKeyORM columnForeignKeyORM, Object object) {
		Collection<Object> objects = new ArrayList<>();
		
		try {			
			Map<ColumnORM, Object> columnsWhere = new HashMap<>();
			
			for(ColumnORM column : columnForeignKeyORM.getTableAssociation().getColumnsPrimaryKeys().values()) {
				columnsWhere.put(column, null);
			}
			
			LOG.debug("executeReadInTableAssociation columns where : " + columnsWhere);
			
			String commandSQLRead = columnForeignKeyORM.getTableAssociation().getReadCommandSQL(
					columnForeignKeyORM.getTableAssociation().getColumnsForeignKeys(), 
					columnsWhere);
			
			LOG.debug("executeReadInTableAssociation command SQL read : " + commandSQLRead);
			
			if(UtilORM.isNullOrEmpty(commandSQLRead)) return objects;
			
			PreparedStatement ps = this.connection.prepareStatement(commandSQLRead);
			
			if(ps == null) return objects;
			
			int parameterIndex = 1;
			
			for(ColumnORM column : columnsWhere.keySet()) {
				LOG.debug("executeReadInTableAssociation column read : " + column);
				if(column == null) continue;
				
				Object value = column.getValue(object);
				LOG.debug("executeReadInTableAssociation value read : " + value);
				
				ps.setObject(parameterIndex, value);
				parameterIndex++;
			}
			
			ResultSet result = ps.executeQuery();
			
			if(result == null) return objects;
			
			//Pour chaque ligne
			while(result.next()) {

				columnsWhere.clear();	
			
				if(columnForeignKeyORM.isPrimitiveValue()) {
					Object valueORM = result.getObject(1);
					LOG.debug("executeReadInTableAssociation value ORM : " + valueORM);	
					
					objects.add(valueORM);
				} 
				else {
					int i = 1;
					for(ColumnORM columnORM : columnForeignKeyORM.getTableAssociation().getColumnsForeignKeys().values()) {
						LOG.debug("executeReadInTableAssociation column primary key ORM : " + columnORM);

						Object valueORM = result.getObject(i);
						LOG.debug("executeReadInTableAssociation value ORM : " + valueORM);	
						
						int j = 1;
						for(ColumnORM columnTarget : columnForeignKeyORM.getTableTarget().getColumnsPrimaryKeys().values()) {
							if(i == j) {
								LOG.debug("executeReadInTableAssociation column target : " + columnTarget);
								LOG.debug("executeReadInTableAssociation value target : " + valueORM);
								columnsWhere.put(columnTarget, valueORM);
							}
							j++;
						}
						
						i++;
					}
					
					Collection<Object> otherObjects = 
							this.executeRead(columnForeignKeyORM.getTableTarget(),
							null,
							columnsWhere);
					
					LOG.debug("executeReadInTableAssociation otherObjects : " + otherObjects);
					objects.addAll(otherObjects);	
				}
			}
			
			result.close();
			ps.close();
			
			LOG.debug("executeReadInTableAssociation close result : " + objects);
			
			return objects;
		} catch(SQLException e) {
			LOG.error(e.getMessage(), e);
			return objects;
		}
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the properties
	 */
	public Properties getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	/**
	 * @return the driver
	 */
	public Driver getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((connection == null) ? 0 : connection.hashCode());
		result = prime * result + ((driver == null) ? 0 : driver.hashCode());
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionJdbcORM other = (SessionJdbcORM) obj;
		if (connection == null) {
			if (other.connection != null)
				return false;
		} else if (!connection.equals(other.connection))
			return false;
		if (driver == null) {
			if (other.driver != null)
				return false;
		} else if (!driver.equals(other.driver))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SessionJdbcORM [url=" + url + ", properties=" + properties + ", driver=" + driver + ", connection="
				+ connection + "]";
	}
}
