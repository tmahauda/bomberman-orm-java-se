package universite.angers.master.info.orm.bd.column;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import org.apache.log4j.Logger;
import universite.angers.master.info.orm.bd.Executable;
import universite.angers.master.info.orm.bd.WrapperType;
import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.util.UtilORM;

/**
 * Classe qui permet d'associer une colonne d'une table de base de données avec l'attribut d'un objet Java
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class ColumnORM implements Executable {

	/**
	 * Log de classe ColumnORM
	 */
	private static final Logger LOG = Logger.getLogger(ColumnORM.class);
	
	/**
	 * Représente l'attribut d'un objet
	 */
	protected Field field;
	
	/**
	 * Représente la classe de l'attribut
	 */
	protected Class<?> clazz;
	
	/**
	 * L'attribut est-il primitive ?
	 */
	protected boolean primitiveValue;
	
	/**
	 * Représente la valeur par défaut à affecter pour les valeurs primitives
	 * En effet les valeurs primitives n'accepte pas la valeur null
	 */
	protected Object defaultValue;
	
	/**
	 * L'attribut est-il une collection ?
	 */
	protected boolean collection;
	
	/**
	 * L'attribut est-il un tableau ?
	 */
	protected boolean array;
	
	/**
	 * Le nom de l'attribut
	 */
	protected String name;
	
	/**
	 * Le type de l'attribut
	 */
	protected String type;
	
	/**
	 * Les contraintes de l'attribut
	 */
	protected String[] constraints;
	
	public ColumnORM(Field field, Class<?> clazz, String name, String[] constraints, Object defaultValue) {
		this.field = field;
		LOG.debug("Field : " + this.field);
		
		this.clazz = clazz;
		LOG.debug("Clazz : " + this.clazz);
		
		this.collection = false;
		LOG.debug("Collection : " + this.collection);
		
		this.array = false;
		LOG.debug("Array : " + this.array);
		
		this.name = name;
		LOG.debug("Name : " + this.name);
		
		this.type = this.initType();
		LOG.debug("Type : " + this.type);
		
		this.constraints = constraints;
		LOG.debug("Constraints : " + Arrays.toString(this.constraints));
		
		this.primitiveValue = WrapperType.isPrimitive(this.clazz);
		LOG.debug("Primitive value : " + this.primitiveValue);
		
		this.defaultValue = defaultValue;
		LOG.debug("Default value : " + this.defaultValue);
	}
	
	public ColumnORM(Field field) {
		this.field = field;
		LOG.debug("Field : " + this.field);
		
		this.clazz = this.initClazz();
		LOG.debug("Clazz : " + this.clazz);
		
		this.name = this.initName();
		LOG.debug("Name : " + this.name);
		
		this.type = this.initType();
		LOG.debug("Type : " + this.type);
		
		this.constraints = this.initConstraints();
		LOG.debug("Constraints : " + Arrays.toString(this.constraints));
		
		this.primitiveValue = WrapperType.isPrimitive(this.clazz);
		LOG.debug("Primitive value : " + this.primitiveValue);
		
		this.defaultValue = this.initDefaultValue();
		LOG.debug("Default value : " + this.defaultValue);
	}
	
	//Les types à redefinir selon la syntaxe utilisé dans la BD
	public abstract String getTypeBoolean();
	public abstract String getTypeByte();
	public abstract String getTypeShort();
	public abstract String getTypeInteger();
	public abstract String getTypeLong();
	public abstract String getTypeFloat();
	public abstract String getTypeDouble();
	public abstract String getTypeCharacter();
	public abstract String getTypeString();
	public abstract String getTypeDate();
	
	/**
	 * Récupère la classe du champ
	 * @return
	 */
	private Class<?> initClazz() {
		//Si c'est une collection
		if(Collection.class.isAssignableFrom(this.field.getType())) {
			this.collection = true;
			this.array = false;
			ParameterizedType typeGeneric = (ParameterizedType) this.field.getGenericType();
			return (Class<?>) typeGeneric.getActualTypeArguments()[0];
		}
		//Si c'est un array
		else if(this.field.getType().isArray()) {
			this.collection = false;
			this.array = true;
			return this.field.getType().getComponentType();
		} 
		else {
			this.collection = false;
			this.array = false;
			return this.field.getType();
		}
	}
	
	/**
	 * Récupère le nom du champ
	 * @return
	 */
	private String initName() {
		if(!this.field.isAnnotationPresent(Column.class)) {
			if(!this.field.isAnnotationPresent(PrimaryKey.class)) {
				if(!this.field.isAnnotationPresent(ForeignKey.class)) {
					return "";
				} else {
					ForeignKey foreignKey = this.field.getDeclaredAnnotation(ForeignKey.class);
					
					if(!UtilORM.isNullOrEmpty(foreignKey.name()))
						return foreignKey.name();
				}
			} else {
				PrimaryKey primaryKey = this.field.getDeclaredAnnotation(PrimaryKey.class);
				
				if(!UtilORM.isNullOrEmpty(primaryKey.name()))
					return primaryKey.name();
			}
		} else {
			Column column = this.field.getDeclaredAnnotation(Column.class);
			
			if(!UtilORM.isNullOrEmpty(column.name()))
				return column.name();	
		}
		
		return this.field.getName();
	}

	/**
	 * Récupère le type du champ
	 * @return
	 */
	private String initType() {		
		if(WrapperType.isBoolean(this.clazz))
            return this.getTypeBoolean();
		else if(WrapperType.isByte(this.clazz))
            return this.getTypeByte();
		else if(WrapperType.isShort(this.clazz))
            return this.getTypeShort();
		else if(WrapperType.isInteger(this.clazz))
            return this.getTypeInteger();
		else if(WrapperType.isLong(this.clazz))
            return this.getTypeLong();
		else if(WrapperType.isFloat(this.clazz))
            return this.getTypeFloat();
		else if(WrapperType.isDouble(this.clazz))
            return this.getTypeDouble();
		else if(WrapperType.isChar(this.clazz))
	        return this.getTypeCharacter();
        else if(WrapperType.isString(this.clazz))
            return this.getTypeString();
        else if(WrapperType.isDate(this.clazz))
            return this.getTypeDate();
        else if(WrapperType.isEnum(this.clazz))
            return this.getTypeString();
        else
        	return this.clazz.getSimpleName();
	}
	
	/**
	 * Récupère la valeur par défaut
	 * @return
	 */
	private Object initDefaultValue() {
		if(WrapperType.isBoolean(this.clazz))
            return false;
		else if(WrapperType.isByte(this.clazz))
            return (byte)0;
		else if(WrapperType.isShort(this.clazz))
            return (short)0;
		else if(WrapperType.isInteger(this.clazz))
            return (int)0;
		else if(WrapperType.isLong(this.clazz))
            return (long)0;
		else if(WrapperType.isFloat(this.clazz))
            return (float)0;
		else if(WrapperType.isDouble(this.clazz))
            return (double)0;
		else if(WrapperType.isChar(this.clazz))
	        return Character.MIN_VALUE;
        else if(WrapperType.isString(this.clazz))
            return null;
        else if(WrapperType.isDate(this.clazz))
            return null;
        else if(WrapperType.isEnum(this.clazz))
            return null;
        else
        	return null;
	}

	/**
	 * Récupère les contraintes du champ
	 * @return
	 */
	private String[] initConstraints() {
		if(this.field.isAnnotationPresent(Column.class)) {
			Column column = this.field.getDeclaredAnnotation(Column.class);
			return column.constraints();
		} else {
			return new String[] {};
		}
	}
	
	/**
	 * Récupère la valeur d'un champ
	 * @param object l'instance qui contient la valeur
	 * @return la valeur
	 */
	public Object getValue(Object object) {
		if(object == null) return null;
		Object value = null;
		
		//On autorise l'accès à l'attribut, quelle soit private, protected ou public
		this.field.setAccessible(true);
		
		try {
			value = this.field.get(object);
			value = WrapperType.convertWrapperPrimitive(this.field.getType(), value);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		this.field.setAccessible(false);
		return value;
	}
	
	/**
	 * Injecte une valeur dans le champ d'un objet
	 * @param object l'instance
	 * @param value la valeur a injecter
	 * @return
	 */
	public boolean setValue(Object object, Object value) {
		boolean update = false;
		
		//On autorise l'accès aux attributs privés
		this.field.setAccessible(true);
		
		try {
			//Cas des nombres dont le cast n'est pas automatique
			//Can not set float field iut.angers.master.info.orm.model.Boss.salaire to java.lang.Double
			if(WrapperType.isByte(this.field.getType())) {
				this.field.set(object, ((Number)value).byteValue());
			}
			else if(WrapperType.isInteger(this.field.getType())) {
				this.field.set(object, ((Number)value).intValue());
			}
			else if(WrapperType.isShort(this.field.getType())) {
				this.field.set(object, ((Number)value).shortValue());
			}
			else if(WrapperType.isLong(this.field.getType())) {
				this.field.set(object, ((Number)value).longValue());
			}
			else if(WrapperType.isFloat(this.field.getType())) {
				this.field.set(object, ((Number)value).floatValue());
			}
			else if(WrapperType.isDouble(this.field.getType())) {
				this.field.set(object, ((Number)value).doubleValue());
			} 
			else {
				this.field.set(object, WrapperType.convertWrapperPrimitive(this.field.getType(), value));
			}
			
			update = true;
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		//On n'autorise plus l'accès au champ
		this.field.setAccessible(false);
		
		return update;
	}
	
	/**
	 * @return the primitiveValue
	 */
	public boolean isPrimitiveValue() {
		return primitiveValue;
	}

	/**
	 * @param primitiveValue the primitiveValue to set
	 */
	public void setPrimitiveValue(boolean primitiveValue) {
		this.primitiveValue = primitiveValue;
	}

	/**
	 * @return the defaultValue
	 */
	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the field
	 */
	public Field getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(Field field) {
		this.field = field;
	}

	/**
	 * @return the clazz
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * @return the collection
	 */
	public boolean isCollection() {
		return collection;
	}

	/**
	 * @param collection the collection to set
	 */
	public void setCollection(boolean collection) {
		this.collection = collection;
	}

	/**
	 * @return the array
	 */
	public boolean isArray() {
		return array;
	}

	/**
	 * @param array the array to set
	 */
	public void setArray(boolean array) {
		this.array = array;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the constraints
	 */
	public String[] getConstraints() {
		return constraints;
	}

	/**
	 * @param constraints the constraints to set
	 */
	public void setConstraints(String[] constraints) {
		this.constraints = constraints;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (array ? 1231 : 1237);
		result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
		result = prime * result + (collection ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(constraints);
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((field == null) ? 0 : field.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (primitiveValue ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColumnORM other = (ColumnORM) obj;
		if (array != other.array)
			return false;
		if (clazz == null) {
			if (other.clazz != null)
				return false;
		} else if (!clazz.equals(other.clazz))
			return false;
		if (collection != other.collection)
			return false;
		if (!Arrays.equals(constraints, other.constraints))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (field == null) {
			if (other.field != null)
				return false;
		} else if (!field.equals(other.field))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (primitiveValue != other.primitiveValue)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ColumnORM [field=" + field + ", clazz=" + clazz + ", collection=" + collection + ", array=" + array
				+ ", name=" + name + ", type=" + type + ", constraints=" + Arrays.toString(constraints) + "]";
	}
}
