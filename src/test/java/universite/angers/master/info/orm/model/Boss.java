package universite.angers.master.info.orm.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table
public class Boss extends Man {

	@Column
	private float salaire;
	
	/**
	 * Utilisation HashSet
	 */
	@ForeignKey
	private Set<Skill> highLevels;
	
	@ForeignKey
	private Skill[] lowLevels;
	
	@ForeignKey
	private Skill skillMain;
	
	/**
	 * Constructeur par défaut sans argument pour l'ORM
	 */
	public Boss() {
		this(null, null, null, new ArrayList<>(), new Person[0], null, new HashSet<>(), new Skill[0], null, 0, 'M',
				new ArrayList<>(), new String[0], new Date(), Job.MANAGER);
	}
	
	public Boss(String firstName, String lastName, String nickName, List<Person> goodFriends, Person[] badFriends,
			Person bestFriend, Set<Skill> highLevels, Skill[] lowLevels, Skill skillMain, float salaire, char sexe,
			List<String> publicMessages, String[] privateMessages, Date birth, Job job) {
		super(firstName, lastName, nickName, goodFriends, badFriends, bestFriend, sexe, publicMessages, privateMessages, birth, job);
		this.highLevels = highLevels;
		this.lowLevels = lowLevels;
		this.skillMain = skillMain;
		this.salaire = salaire;
	}

	/**
	 * @return the highLevels
	 */
	public Set<Skill> getHighLevels() {
		return highLevels;
	}

	/**
	 * @param highLevels the highLevels to set
	 */
	public void setHighLevels(Set<Skill> highLevels) {
		this.highLevels = highLevels;
	}

	/**
	 * @return the lowLevels
	 */
	public Skill[] getLowLevels() {
		return lowLevels;
	}

	/**
	 * @param lowLevels the lowLevels to set
	 */
	public void setLowLevels(Skill[] lowLevels) {
		this.lowLevels = lowLevels;
	}

	/**
	 * @return the skillMain
	 */
	public Skill getSkillMain() {
		return skillMain;
	}

	/**
	 * @param skillMain the skillMain to set
	 */
	public void setSkillMain(Skill skillMain) {
		this.skillMain = skillMain;
	}

	/**
	 * @return the salaire
	 */
	public float getSalaire() {
		return salaire;
	}

	/**
	 * @param salaire the salaire to set
	 */
	public void setSalaire(float salaire) {
		this.salaire = salaire;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((highLevels == null) ? 0 : highLevels.hashCode());
		result = prime * result + Arrays.hashCode(lowLevels);
		result = prime * result + Float.floatToIntBits(salaire);
		result = prime * result + ((skillMain == null) ? 0 : skillMain.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boss other = (Boss) obj;
		if (highLevels == null) {
			if (other.highLevels != null)
				return false;
		} else if (!highLevels.equals(other.highLevels))
			return false;
		if (!Arrays.equals(lowLevels, other.lowLevels))
			return false;
		if (Float.floatToIntBits(salaire) != Float.floatToIntBits(other.salaire))
			return false;
		if (skillMain == null) {
			if (other.skillMain != null)
				return false;
		} else if (!skillMain.equals(other.skillMain))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Boss [salaire=" + salaire + ", highLevels=" + highLevels + ", lowLevels=" + Arrays.toString(lowLevels)
				+ ", skillMain=" + skillMain + ", sexe=" + sexe + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", nickName=" + nickName + ", publicMessages=" + publicMessages + ", privateMessages="
				+ Arrays.toString(privateMessages) + "]";
	}
}
