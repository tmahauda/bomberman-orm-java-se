package universite.angers.master.info.orm.model;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.ForeignKey;
import universite.angers.master.info.orm.bd.annotation.PrimaryKey;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table(isInstanciable = false)
public abstract class Person implements Comparable<Person> {

	@PrimaryKey
	protected String firstName;
	
	@PrimaryKey
	protected String lastName;
	
	@Column
	protected String nickName;
	
	@Column
	protected Date birth;
	
	@Column
	protected Job job;
	
	/**
	 * Utilisation ArrayList
	 */
	@ForeignKey
	protected List<Person> goodFriends;
	
	@ForeignKey
	protected Person[] badFriends;
	
	@ForeignKey
	protected Person bestFriend;
	
	@ForeignKey
	protected List<String> publicMessages;
	
	@ForeignKey
	protected String[] privateMessages;

	public Person(String firstName, String lastName, String nickName, List<Person> goodFriends, Person[] badFriends,
			Person bestFriend, List<String> publicMessages, String[] privateMessages, Date birth, Job job) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
		this.goodFriends = goodFriends;
		this.badFriends = badFriends;
		this.bestFriend = bestFriend;
		this.publicMessages = publicMessages;
		this.privateMessages = privateMessages;
		this.birth = birth;
		this.job = job;
	}

	@Override
	public int compareTo(Person arg0) {
		return 0;
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @return the goodFriends
	 */
	public List<Person> getGoodFriends() {
		return goodFriends;
	}

	/**
	 * @param goodFriends the goodFriends to set
	 */
	public void setGoodFriends(List<Person> goodFriends) {
		this.goodFriends = goodFriends;
	}

	/**
	 * @return the badFriends
	 */
	public Person[] getBadFriends() {
		return badFriends;
	}

	/**
	 * @param badFriends the badFriends to set
	 */
	public void setBadFriends(Person[] badFriends) {
		this.badFriends = badFriends;
	}

	/**
	 * @return the bestFriend
	 */
	public Person getBestFriend() {
		return bestFriend;
	}

	/**
	 * @param bestFriend the bestFriend to set
	 */
	public void setBestFriend(Person bestFriend) {
		this.bestFriend = bestFriend;
	}

	/**
	 * @return the publicMessages
	 */
	public List<String> getPublicMessages() {
		return publicMessages;
	}

	/**
	 * @param publicMessages the publicMessages to set
	 */
	public void setPublicMessages(List<String> publicMessages) {
		this.publicMessages = publicMessages;
	}

	/**
	 * @return the privateMessages
	 */
	public String[] getPrivateMessages() {
		return privateMessages;
	}

	/**
	 * @param privateMessages the privateMessages to set
	 */
	public void setPrivateMessages(String[] privateMessages) {
		this.privateMessages = privateMessages;
	}

	/**
	 * @return the birth
	 */
	public Date getBirth() {
		return birth;
	}

	/**
	 * @param birth the birth to set
	 */
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	
	/**
	 * @return the job
	 */
	public Job getJob() {
		return job;
	}

	/**
	 * @param job the job to set
	 */
	public void setJob(Job job) {
		this.job = job;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(badFriends);
		result = prime * result + ((bestFriend == null) ? 0 : bestFriend.hashCode());
		result = prime * result + ((birth == null) ? 0 : birth.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((goodFriends == null) ? 0 : goodFriends.hashCode());
		result = prime * result + ((job == null) ? 0 : job.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((nickName == null) ? 0 : nickName.hashCode());
		result = prime * result + Arrays.hashCode(privateMessages);
		result = prime * result + ((publicMessages == null) ? 0 : publicMessages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (!Arrays.equals(badFriends, other.badFriends))
			return false;
		if (bestFriend == null) {
			if (other.bestFriend != null)
				return false;
		} else if (!bestFriend.equals(other.bestFriend))
			return false;
		if (birth == null) {
			if (other.birth != null)
				return false;
		} else if (!birth.equals(other.birth))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (goodFriends == null) {
			if (other.goodFriends != null)
				return false;
		} else if (!goodFriends.equals(other.goodFriends))
			return false;
		if (job != other.job)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (nickName == null) {
			if (other.nickName != null)
				return false;
		} else if (!nickName.equals(other.nickName))
			return false;
		if (!Arrays.equals(privateMessages, other.privateMessages))
			return false;
		if (publicMessages == null) {
			if (other.publicMessages != null)
				return false;
		} else if (!publicMessages.equals(other.publicMessages))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", nickName=" + nickName + ", birth="
				+ birth + ", job=" + job + ", goodFriends=" + goodFriends + ", badFriends="
				+ Arrays.toString(badFriends) + ", bestFriend=" + bestFriend + ", publicMessages=" + publicMessages
				+ ", privateMessages=" + Arrays.toString(privateMessages) + "]";
	}
}
