package universite.angers.master.info.orm.model;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import universite.angers.master.info.orm.bd.annotation.Column;
import universite.angers.master.info.orm.bd.annotation.Table;

@Table(isInstanciable = false)
public class Man extends Person {

	@Column
	protected char sexe;
	
	public Man(String firstName, String lastName, String nickName, List<Person> goodFriends, Person[] badFriends,
			Person bestFriend, char sexe, List<String> publicMessages, String[] privateMessages, Date birth, Job job) {
		super(firstName, lastName, nickName, goodFriends, badFriends, bestFriend, publicMessages, privateMessages, birth, job);
		this.sexe = sexe;
	}

	/**
	 * @return the sexe
	 */
	public char getSexe() {
		return sexe;
	}

	/**
	 * @param sexe the sexe to set
	 */
	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + sexe;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Man other = (Man) obj;
		if (sexe != other.sexe)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Man [sexe=" + sexe + ", firstName=" + firstName + ", lastName=" + lastName + ", nickName=" + nickName
				+ ", birth=" + birth + ", publicMessages=" + publicMessages + ", privateMessages="
				+ Arrays.toString(privateMessages) + "]";
	}
}
